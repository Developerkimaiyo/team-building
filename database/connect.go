package database

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	models "sendy.com/intern/api/model"
)

var DB *gorm.DB

func Connect() {
	database, err := gorm.Open(mysql.Open("root:password@/SendyIntern"), &gorm.Config{})

	if err != nil {
		panic("Could not connect to the database")
	}

	DB = database

	database.AutoMigrate(&models.Intern{}, &models.Stack{})
}
