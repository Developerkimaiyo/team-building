package models

import (
	"time"
)

type Intern struct {
	Id        uint `json:"id" gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	Name      string  `json:"name"`
	Email     string  `json:"email" gorm:"unique"`
	Month     string  `json:"month"`
	Year      string  `json:"year"`
	Stack     []Stack `json.Array:"stack" gorm:"many2many:intern_stack;"`
}

type Stack struct {
	Id        uint `json:"id" gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	Name      string `json:"name"`
}
