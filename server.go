package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"sendy.com/intern/api/database"
	"sendy.com/intern/api/routes"
)

func main() {
	app := fiber.New()
	database.Connect()

	app.Use(cors.New(cors.Config{
		AllowCredentials: true,
	}))

	routes.Setup(app)

	app.Listen(":3000")
}
