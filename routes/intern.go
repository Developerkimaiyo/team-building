package routes

import (
	"github.com/gofiber/fiber/v2"
	"sendy.com/intern/api/controller"
)

func Setup(app *fiber.App) {

	app.Get("/api/intern", controller.AllIntern)
	app.Post("/api/intern", controller.CreateIntern)
	app.Get("/api/intern/:id", controller.GetIntern)
	app.Put("/api/intern/:id", controller.UpdateIntern)
	app.Delete("/api/intern/:id", controller.DeleteIntern)
}
