package controller

import (
	"strconv"

	"github.com/gofiber/fiber/v2"
	"sendy.com/intern/api/database"
	models "sendy.com/intern/api/model"
)

func AllIntern(c *fiber.Ctx) error {
	var interns models.Intern
	intern := database.DB.Find(&interns)

	return c.JSON(intern)
}

func CreateIntern(c *fiber.Ctx) error {
	var intern models.Intern

	if err := c.BodyParser(&intern); err != nil {
		return err
	}

	database.DB.Create(&intern)

	return c.JSON(intern)
}

func GetIntern(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	intern := models.Intern{
		Id: uint(id),
	}

	database.DB.Find(&intern)

	return c.JSON(intern)
}

func UpdateIntern(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	intern := models.Intern{
		Id: uint(id),
	}

	if err := c.BodyParser(&intern); err != nil {
		return err
	}

	database.DB.Model(&intern).Updates(intern)

	return c.JSON(intern)
}

func DeleteIntern(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	intern := models.Intern{
		Id: uint(id),
	}

	database.DB.Delete(&intern)

	return nil
}
